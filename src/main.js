import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import {PerfAnalyticsLibrary} from "perf-analytics-lib/dist";
import {serverAddress} from "./config";

Vue.config.productionTip = false;

const PerfAnalytics=new PerfAnalyticsLibrary(serverAddress);
PerfAnalytics.run();


new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app');
